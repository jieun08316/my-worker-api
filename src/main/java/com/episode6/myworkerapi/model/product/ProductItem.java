package com.episode6.myworkerapi.model.product;

import com.episode6.myworkerapi.entity.Product;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductItem {
    private Long id;
    private Long businessMemberId;
    private String businessMemberName;
    private LocalDateTime dateProduct;
    private String productName;
    private Short minQuantity;
    private Short nowQuantity;

    private ProductItem(Builder builder) {
        this.id = builder.id;
        this.businessMemberId = builder.businessMemberId;
        this.businessMemberName = builder.businessMemberName;
        this.dateProduct = builder.dateProduct;
        this.productName = builder.productName;
        this.minQuantity = builder.minQuantity;
        this.nowQuantity = builder.nowQuantity;
    }

    public static class Builder implements CommonModelBuilder<ProductItem> {
        private final Long id;
        private final Long businessMemberId;
        private final String businessMemberName;
        private final LocalDateTime dateProduct;
        private final String productName;
        private final Short minQuantity;
        private final Short nowQuantity;

        public Builder(Product product) {
            this.id = product.getId();
            this.businessMemberId = product.getBusinessMember().getId();
            this.businessMemberName = product.getBusinessMember().getMember().getName();
            this.dateProduct = product.getDateProduct();
            this.productName = product.getProductName();
            this.minQuantity = product.getMinQuantity();
            this.nowQuantity = product.getNowQuantity();
        }

        @Override
        public ProductItem build() {
            return new ProductItem(this);
        }
    }
}
