package com.episode6.myworkerapi.model.manual;


import com.episode6.myworkerapi.entity.Manual;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ManualItem {
    private Long id;
    private Long businessMemberId;
    private LocalDate dateManual;
    private String title;
    private String content;


    private ManualItem(Builder builder) {
        this.id = builder.id;
        this.businessMemberId = builder.businessMemberId;
        this.dateManual = builder.dateManual;
        this.title = builder.title;
        this.content = builder.content;
    }
    public static class Builder implements CommonModelBuilder<ManualItem> {

        private Long id;
        private Long businessMemberId;
        private LocalDate dateManual;
        private String title;
        private String content;

        public Builder(Manual manual) {
            this.id = manual.getId();
            this.businessMemberId = manual.getBusinessMember().getId();
            this.dateManual = manual.getDateManual();
            this.title = manual.getTitle();
            this.content = manual.getContent();
        }

        @Override
        public ManualItem build() {
            return new ManualItem(this);
        }
    }
}
