package com.episode6.myworkerapi.model.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefuseReasonRequest {

    private String refuseReason;
}
