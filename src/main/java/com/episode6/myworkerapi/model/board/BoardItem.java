package com.episode6.myworkerapi.model.board;

import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.Category;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardItem {
    private Long id;
    private Category category;
    private String title;
    private String content;
    private String boardImgUrl;
    private LocalDateTime dateBoard;

    private Long memberId;

    private BoardItem(Builder builder) {
        this.id = builder.id;
        this.category = builder.category;
        this.title = builder.title;
        this.content = builder.content;
        this.dateBoard = builder.dateBoard;
        this.boardImgUrl = builder.boardImgUrl;
        this.memberId = builder.memberId;

    }

    public static class Builder implements CommonModelBuilder<BoardItem> {
        private final Long id;
        private final Category category;
        private final String title;
        private final String content;
        private final String boardImgUrl;
        private final LocalDateTime dateBoard;
        private final Long memberId;

        public Builder(Board board) {
            this.id = board.getId();
            this.category = board.getCategory();
            this.title = board.getTitle();
            this.content = board.getContent();
            this.boardImgUrl = board.getBoardImgUrl();
            this.dateBoard = board.getDateBoard();
            this.memberId = board.getMember().getId();
        }
        @Override
        public BoardItem build() {
            return new BoardItem(this);
        }
    }
}
