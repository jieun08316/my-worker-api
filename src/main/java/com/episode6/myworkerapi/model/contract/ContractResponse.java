package com.episode6.myworkerapi.model.contract;



import com.episode6.myworkerapi.entity.Contract;
import com.episode6.myworkerapi.enums.PayType;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ContractResponse {
    private Long id;
    private Long businessMemberId;
    private LocalDate dateContract;
    private PayType payType;
    private Double payPrice;
    private LocalDate dateWorkStart;
    private LocalDate dateWorkEnd;
    private Short restTime;
    private String insurance;
    private String workDay;
    private Short workTime;
    private Short weekWorkTime;
    private Boolean isWeekPay;
    private Double mealPay;
    private String wagePayment;
    private String wageAccountNumber;
    private String contractCopyImgUrl;

    private ContractResponse(Builder builder){
        this.id = builder.id;
        this.businessMemberId = builder.businessMemberId;
        this.dateContract = builder.dateContract;
        this.payType = builder.payType;
        this.payPrice = builder.payPrice;
        this.dateWorkStart = builder.dateWorkStart;
        this.dateWorkEnd = builder.dateWorkEnd;
        this.restTime = builder.restTime;
        this.insurance = builder.insurance;
        this.workDay = builder.workDay;
        this.workTime = builder.workTime;
        this.weekWorkTime = builder.weekWorkTime;
        this.isWeekPay = builder.isWeekPay;
        this.mealPay = builder.mealPay;
        this.wagePayment = builder.wagePayment;
        this.wageAccountNumber = builder.wageAccountNumber;
        this.contractCopyImgUrl = builder.contractCopyImgUrl;
    }


    public static class Builder implements CommonModelBuilder<ContractResponse> {
        private final Long id;
        private final Long businessMemberId;
        private final LocalDate dateContract;
        private final PayType payType;
        private final Double payPrice;
        private final LocalDate dateWorkStart;
        private final LocalDate dateWorkEnd;
        private final Short restTime;
        private final String insurance;
        private final String workDay;
        private final Short workTime;
        private final Short weekWorkTime;
        private final Boolean isWeekPay;
        private final Double mealPay;
        private final String wagePayment;
        private final String wageAccountNumber;
        private final String contractCopyImgUrl;

        public Builder(Contract contract) {

            this.id = contract.getId();
            this.businessMemberId = contract.getBusinessMember().getId();
            this.dateContract = contract.getDateContract();
            this.payType = contract.getPayType();
            this.payPrice = contract.getPayPrice();
            this.dateWorkStart = contract.getDateWorkStart();
            this.dateWorkEnd = contract.getDateWorkEnd();
            this.restTime = contract.getRestTime();
            this.insurance = contract.getInsurance().getName();
            this.workDay = contract.getWorkDay();
            this.workTime = contract.getWorkTime();
            this.weekWorkTime = contract.getWeekWorkTime();
            this.isWeekPay = contract.getIsWeekPay();
            this.mealPay = contract.getMealPay();
            this.wagePayment = contract.getWagePayment();
            this.wageAccountNumber = contract.getWageAccountNumber();
            this.contractCopyImgUrl = contract.getContractCopyImgUrl();
        }
        @Override
        public ContractResponse build() {
            return new ContractResponse(this);
        }
    }
}
