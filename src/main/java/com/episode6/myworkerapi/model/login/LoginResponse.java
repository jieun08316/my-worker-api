package com.episode6.myworkerapi.model.login;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponse {
    private String token;
    private String name;

    private LoginResponse(LoginResponseBuilder builder) {
        this.token = builder.token;
        this.name = builder.name;
    }

    public static class LoginResponseBuilder implements CommonModelBuilder<LoginResponse> {
        private final String token;
        private final String name;

        public LoginResponseBuilder(String token, String name) {
            this.token = token;
            this.name = name;
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
