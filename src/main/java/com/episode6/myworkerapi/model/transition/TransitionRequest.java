package com.episode6.myworkerapi.model.transition;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class TransitionRequest {
    private String content;
}
