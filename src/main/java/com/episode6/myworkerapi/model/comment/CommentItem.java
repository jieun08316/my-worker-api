package com.episode6.myworkerapi.model.comment;


import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.entity.Comment;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CommentItem {
    private Long id;
    private String content;
    private LocalDateTime dateContent;

    private Long memberId;

    private Long boardId;

    private CommentItem(Builder builder) {
        this.id = builder.id;
        this.content = builder.content;
        this.dateContent = builder.dateContent;
        this.memberId = builder.memberId;
        this.boardId = builder.boardId;
    }

    public static class Builder implements CommonModelBuilder<CommentItem> {
        private final Long id;
        private final String content;
        private final LocalDateTime dateContent;

        private final Long memberId;

        private final Long boardId;

        public Builder(Comment comment) {
            this.id = comment.getId();
            this.content = comment.getContent();
            this.dateContent = comment.getDateContent();
            this.memberId = comment.getMember().getId();
            this.boardId = comment.getBoard().getId();
        }
        @Override
        public CommentItem build() {
            return new CommentItem();
        }
    }
}
