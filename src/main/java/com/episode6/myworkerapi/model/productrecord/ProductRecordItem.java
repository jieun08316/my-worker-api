package com.episode6.myworkerapi.model.productrecord;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Product;
import com.episode6.myworkerapi.entity.ProductRecord;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductRecordItem {
    private Long id;
    private Long productId;
    private String productName;
    private Long businessId;
    private String businessName;
    private Long businessMemberId;
    private String businessMemberName;
    private Short nowQuantity;
    private LocalDateTime dateProductRecord;

    private ProductRecordItem(Builder builder) {
        this.id = builder.id;
        this.productId = builder.productId;
        this.productName = builder.productName;
        this.businessId = builder.businessId;
        this.businessName = builder.businessName;
        this.businessMemberId = builder.businessMemberId;
        this.businessMemberName = builder.businessMemberName;
        this.nowQuantity = builder.nowQuantity;
        this.dateProductRecord = builder.dateProductRecord;
    }

    public static class Builder implements CommonModelBuilder<ProductRecordItem> {
        private final Long id;
        private final Long productId;
        private final String productName;
        private final Long businessId;
        private final String businessName;
        private final Long businessMemberId;
        private final String businessMemberName;
        private final Short nowQuantity;
        private final LocalDateTime dateProductRecord;

        public Builder(ProductRecord productRecord) {
            this.id = productRecord.getId();
            this.productId = productRecord.getProduct().getId();
            this.productName = productRecord.getProduct().getProductName();
            this.businessId = productRecord.getBusinessMember().getBusiness().getId();
            this.businessName = productRecord.getBusinessMember().getBusiness().getBusinessName();
            this.businessMemberId = productRecord.getBusinessMember().getId();
            this.businessMemberName = productRecord.getBusinessMember().getMember().getName();
            this.nowQuantity = productRecord.getNowQuantity();
            this.dateProductRecord = productRecord.getDateProductRecord();
        }

        @Override
        public ProductRecordItem build() {
            return new ProductRecordItem(this);
        }
    }
}
