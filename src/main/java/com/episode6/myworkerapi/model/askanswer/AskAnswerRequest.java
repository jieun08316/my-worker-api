package com.episode6.myworkerapi.model.askanswer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AskAnswerRequest {
    private String answerTitle;
    private String answerContent;
    private String answerImgUrl;
}
