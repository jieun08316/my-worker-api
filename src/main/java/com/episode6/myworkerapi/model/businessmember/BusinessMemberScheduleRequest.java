package com.episode6.myworkerapi.model.businessmember;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessMemberScheduleRequest {
    private String weekSchedule;
    private String timeScheduleStart;
    private String timeScheduleEnd;
    private String timeScheduleTotal;
    private String timeScheduleRest;
}
