package com.episode6.myworkerapi.model.report;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportRequest {
    private String content;

}
