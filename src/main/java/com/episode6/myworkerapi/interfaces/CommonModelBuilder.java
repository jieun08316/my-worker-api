package com.episode6.myworkerapi.interfaces;

import com.episode6.myworkerapi.entity.Board;

public interface CommonModelBuilder<T> {
    T build();
}
