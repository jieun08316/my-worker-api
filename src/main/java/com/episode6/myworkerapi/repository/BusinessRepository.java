package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusinessRepository extends JpaRepository<Business, Long> {

    /**
     * 사업장 최신순
     */
    Page<Business> findByOrderByIdDesc(Pageable pageable);


        Business findByIdOrderByDateApprovalBusiness(Long id);
}
