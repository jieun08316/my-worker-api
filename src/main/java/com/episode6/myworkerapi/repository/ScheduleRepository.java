package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Schedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    Optional<Schedule> findByBusinessMemberAndDateWork(BusinessMember businessMember, LocalDate localDate);

    Page<Schedule> findAllByOrderByIdDesc(Pageable pageable);
}
