package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Contract;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.contract.ContractRequest;
import com.episode6.myworkerapi.model.contract.ContractResponse;
import com.episode6.myworkerapi.repository.ContractRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ContractService {
    private final ContractRepository contractRepository;

    /**
     * 계약 등록
     */
    public void setContract(BusinessMember businessMember, ContractRequest request) {
        contractRepository.save(new Contract.Builder(request,businessMember).build());
    }

    /**
     * 계약 상세보기
     */
    public ContractResponse getContract(long id) {
        Contract contract = contractRepository.findById(id).orElseThrow();
        return new ContractResponse.Builder(contract).build();
    }
    /**
     * 계약 페이징 최신순
     */
    public ListResult<ContractResponse> getContracts(int pageNum){
        PageRequest pageRequest = PageRequest.of(pageNum-1,10);

        Page<Contract> responsePage = contractRepository.findAllByOrderByIdDesc(pageRequest);

        List<ContractResponse> responseList = new LinkedList<>();
        for (Contract contract : responsePage.getContent()) {
            responseList.add(new ContractResponse.Builder(contract).build());
        }
        ListResult<ContractResponse> result = new ListResult<>();
        result.setList(responseList);
        result.setTotalCount(responsePage.getTotalElements());
        result.setTotalPage(responsePage.getTotalPages());
        result.setCurrentPage(responsePage.getPageable().getPageNumber() + 1);

        return result;
    }

}
