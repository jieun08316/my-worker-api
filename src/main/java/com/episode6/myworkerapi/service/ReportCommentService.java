package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.*;
import com.episode6.myworkerapi.enums.ResultCode;
import com.episode6.myworkerapi.exception.CMemberPasswordException;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.report.ReportCommentItem;
import com.episode6.myworkerapi.model.report.ReportRequest;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.repository.ReportCommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class ReportCommentService {
    private final ReportCommentRepository reportCommentRepository;

    /**
     * 댓글 신고 등록 중복 불가능
     */
//    public CommonResult setReportComment(Member member, Comment comment, ReportRequest request) {
//
//
//        Optional<ReportComment> optionalReportComment = reportCommentRepository.findById(comment.getId());
//        CommonResult result = new CommonResult();
//
//        if (optionalReportComment.isEmpty()) {
//            ReportComment addData = new ReportComment();
//            addData.setMember(member);
//            addData.setComment(comment);
//            addData.setContent(request.getContent());
//            addData.setDateReport(LocalDateTime.now());
//
//            reportCommentRepository.save(addData);
//
//            result.setCode(ResultCode.SUCCESS.getCode());
//            result.setMsg(ResultCode.SUCCESS.getMsg());
//        } else  {
//            result.setCode(ResultCode.OVERLAP.getCode());
//            result.setMsg(ResultCode.OVERLAP.getMsg());
//        }
//        return result;
//    }

    /**
     * 댓글 신고 최신순 페이징
     */
    public ListResult<ReportCommentItem> setReportCommentPage(int pageNum) {

        PageRequest pageRequest = PageRequest.of(pageNum -1 , 10);

        Page<ReportComment> reportcommentPage = reportCommentRepository.findAllByOrderByIdDesc(pageRequest);

        List<ReportCommentItem> reportItemList = new LinkedList<>();
        for (ReportComment reportComment : reportcommentPage.getContent()) {
            reportItemList.add(new ReportCommentItem.Builder(reportComment).build());
        }
        return ListConvertService.settingResult(reportItemList);
    }

    /**
     * 댓글 신고 삭제
     */
    public void delReportComment(long id) {
        reportCommentRepository.deleteById(id);
    }
}
