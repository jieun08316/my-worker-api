package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Schedule;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.schedule.ScheduleItem;
import com.episode6.myworkerapi.model.schedule.ScheduleRequest;
import com.episode6.myworkerapi.repository.ScheduleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ScheduleService {
    private final ScheduleRepository scheduleRepository;

    /**
     *
     * @param businessMember 사업장 알바
     * @param request 출근 날짜 시간 등록
     */
    public void setSchedule(BusinessMember businessMember, ScheduleRequest request) {
        Optional<Schedule> schedule = scheduleRepository.findByBusinessMemberAndDateWork(businessMember, LocalDate.now());
        LocalTime toTime = LocalTime.now();
        boolean bool;
        bool = toTime.isAfter(LocalTime.parse(businessMember.getTimeScheduleStart()));
        if (schedule.isEmpty()) scheduleRepository.save(new Schedule.Builder(businessMember, request, bool).build());
    }
//    일요일	Sunday
//    월요일	Monday
//    화요일	Tuesday
//    수요일	Wednesday
//    목요일	Thursday
//    금요일	Friday
//    토요일	Saturday

    /**
     * 출근 리스트 관리자용
     */
    public ListResult<ScheduleItem> getSchedules(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Schedule> schedules = scheduleRepository.findAllByOrderByIdDesc(pageRequest);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result, schedules.getTotalElements(), schedules.getTotalPages(), schedules.getPageable().getPageNumber());
    }
}
