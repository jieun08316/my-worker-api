package com.episode6.myworkerapi.service;


import com.episode6.myworkerapi.configure.JwtTokenProvider;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.MemberType;
import com.episode6.myworkerapi.exception.CMemberPasswordException;
import com.episode6.myworkerapi.model.login.LoginRequest;
import com.episode6.myworkerapi.model.login.LoginResponse;
import com.episode6.myworkerapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    public LoginResponse doLogin(MemberType memberType, LoginRequest loginRequest, String loginType){
        Member member =memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CMemberPasswordException::new);
        if (!member.getMemberType().equals(memberType)) throw new CMemberPasswordException();
        //일반회원이 최고관리자용으로 로그인하려거나 이런 경우이므로 메세지는 회원정보가 없습니다로 일단 던짐.
        if (!passwordEncoder.matches(loginRequest.getPassword(),member.getPassword())) throw new CMemberPasswordException();
        // 비밀번호 일치하지 않습니다 던짐
        String token = jwtTokenProvider.createToken(String.valueOf(member.getUsername()),member.getMemberType().toString(), loginType);
        return new LoginResponse.LoginResponseBuilder(token, member.getName()).build();
    }


}
