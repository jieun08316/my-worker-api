package com.episode6.myworkerapi.entity;


import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.request.FixRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RequestFix {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessId" ,nullable = false)
    private Business business;

    @Column(length = 30)
    private String businessName;

    @Column(length = 20)
    private String ownerName;

    private String businessImgUrl;

    @Column(length = 30)
    private String businessType;

    @Column(length = 60)
    private String businessLocation;

    @Column(length = 50)
    private String businessEmail;

    @Column(length = 13)
    private String businessPhoneNumber;

    @Column(length = 60)
    private String reallyLocation;

    @Column(length = 350)
    private String etcMemo;




    public RequestFix(Builder builder) {
        this.business = builder.business;
        this.businessName = builder.businessName;
        this.ownerName = builder.ownerName;
        this.businessImgUrl = builder.businessImgUrl;
        this.businessType = builder.businessType;
        this. businessLocation = builder.businessLocation;
        this.businessEmail = builder.businessEmail;
        this. businessPhoneNumber = builder.businessPhoneNumber;
        this.reallyLocation = builder.reallyLocation;
        this.etcMemo = builder.etcMemo;
    }

    public static class Builder implements CommonModelBuilder<RequestFix> {
        private final Business business;
        private final String businessName;
        private final String ownerName;
        private final String businessImgUrl;
        private final String businessType;
        private final String businessLocation;
        private final String businessEmail;
        private final String businessPhoneNumber;
        private final String reallyLocation;
        private final String etcMemo;

        public Builder(FixRequest fixRequest, Business business) {
            ArrayList<String> fixMemos = new ArrayList<>();
            if (!business.getBusinessName().equals(fixRequest.getBusinessName())) fixMemos.add("기존 사업장명 : " + business.getBusinessName());
            if (!business.getOwnerName().equals(fixRequest.getOwnerName())) fixMemos.add("기존 대표명 : " + business.getOwnerName());
            if (!business.getBusinessImgUrl().equals(fixRequest.getBusinessImgUrl())) fixMemos.add("기존 사업장 이미지 : " + business.getBusinessImgUrl());
            if (!business.getBusinessType().equals(fixRequest.getBusinessType())) fixMemos.add("기존 사업장 업종 : " + business.getBusinessType());
            if (!business.getBusinessLocation().equals(fixRequest.getBusinessLocation())) fixMemos.add("기존 사업장 소재지 : " + business.getBusinessLocation());
            if (!business.getBusinessEmail().equals(fixRequest.getBusinessEmail())) fixMemos.add("기존 사업자 이메일 : " + business.getBusinessEmail());
            if (!business.getBusinessPhoneNumber().equals(fixRequest.getBusinessPhoneNumber())) fixMemos.add("기존 사업장 전화번호 : " + business.getBusinessPhoneNumber());
            if (!business.getReallyLocation().equals(fixRequest.getReallyLocation())) fixMemos.add("기존 사업장 실근무지 : " + business.getReallyLocation());


            String resultFixMemo = String.join(",", fixMemos); // "사업장명 수정"

            this.business = business;
            this.businessName = fixRequest.getBusinessName();
            this.ownerName = fixRequest.getOwnerName();
            this.businessImgUrl = fixRequest.getBusinessImgUrl();
            this.businessType = fixRequest.getBusinessType();
            this. businessLocation = fixRequest.getBusinessLocation();
            this.businessEmail = fixRequest.getBusinessEmail();
            this. businessPhoneNumber = fixRequest.getBusinessPhoneNumber();
            this.reallyLocation = fixRequest.getReallyLocation();
            this.etcMemo = resultFixMemo;

        }

        @Override
        public RequestFix build() {
            return new RequestFix(this);
        }
    }

}
