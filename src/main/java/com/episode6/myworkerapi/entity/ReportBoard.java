package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.report.ReportRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReportBoard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "boardId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Board board;

    @JoinColumn(name = "memberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Member member;

    private String content;

    @Column(nullable = false)
    private LocalDateTime dateReport;


    private ReportBoard(Builder builder) {
        this.board = builder.board;
        this.member = builder.member;
        this.content = builder.content;
        this.dateReport = builder.dateReport;
    }

    public static class Builder implements CommonModelBuilder<ReportBoard> {
        private final Board board;
        private final Member member;
        private final String content;
        private final LocalDateTime dateReport;

        public Builder(ReportRequest request,Board board,Member member) {
            this.board = board;
            this.member = member;
            this.content = request.getContent();
            this.dateReport = LocalDateTime.now();
        }
        @Override
        public ReportBoard build() {
            return new ReportBoard(this);
        }
    }
}
