package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.enums.MemberState;
import com.episode6.myworkerapi.enums.MemberType;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.member.MemberChangeRequest;
import com.episode6.myworkerapi.model.member.MemberPasswordCheckRequest;
import com.episode6.myworkerapi.model.member.MemberRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.catalina.UserDatabase;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private MemberState memberState;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private MemberType memberType;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 40, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private Boolean isMan;

    @Column(nullable = false)
    private LocalDate dateBirth;

    @Column(nullable = false, length = 13)
    private String phoneNumber;

    @Column(length = 40)
    private String address;

    @Column(nullable = false)
    private LocalDateTime dateMember;

    private LocalDateTime dateChangeMember;

    private LocalDateTime dateOutMember;

    public void putMemberChange(MemberChangeRequest request) {
        this.name = request.getName();
        this.phoneNumber = request.getPhoneNumber();
        this.address = request.getAddress();
    }

    public void putPassword(MemberPasswordCheckRequest request) {
        this.password = request.getPassword();
    }

    private Member(Builder builder) {
        this.memberState = builder.memberState;
        this.memberType = builder.memberType;
        this.name = builder.name;
        this.username = builder.username;
        this.password = builder.password;
        this.isMan = builder.isMan;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.dateMember = builder.dateMember;
        this.dateChangeMember = builder.dateChangeMember;
        this.dateOutMember = builder.dateOutMember;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberType.toString()));
    }

    @Override
    public boolean isAccountNonExpired() { // 만료
        return true;
    }

    @Override
    public boolean isAccountNonLocked() { // 정지
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {  //자격
         return true;
    }

    @Override
    public boolean isEnabled() {  //휴면한 ?
        return true;
    }

    public static class Builder implements CommonModelBuilder<Member> {
        private final MemberState memberState;
        private final MemberType memberType;
        private final String name;
        private final String username;
        private final String password;
        private final Boolean isMan;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String address;
        private final LocalDateTime dateMember;
        private final LocalDateTime dateChangeMember;
        private final LocalDateTime dateOutMember;

        public Builder(MemberRequest request) {
            this.memberState = request.getMemberState();
            this.memberType = request.getMemberType();
            this.name = request.getName();
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.isMan = request.getIsMan();
            this.dateBirth = request.getDateBirth();
            this.phoneNumber = request.getPhoneNumber();
            this.address = request.getAddress();
            this.dateMember = LocalDateTime.now();
            this.dateChangeMember = null;
            this.dateOutMember = null;
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }

    private Member(BuilderCsv builder) {
        this.memberState = builder.memberState;
        this.memberType = builder.memberType;
        this.name = builder.name;
        this.username = builder.username;
        this.password = builder.password;
        this.isMan = builder.isMan;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.dateMember = builder.dateMember;
    }

    public static class BuilderCsv implements CommonModelBuilder<Member> {
        private final MemberState memberState;
        private final MemberType memberType;
        private final String name;
        private final String username;
        private final String password;
        private final Boolean isMan;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final LocalDateTime dateMember;


        public BuilderCsv(String[] cols) {
            this.memberState = (MemberState.valueOf(cols[0]));
            this.memberType = (MemberType.valueOf(cols[1]));
            this.name = (cols[2]);
            this.username = (cols[3]);
            this.password = (cols[4]);
            this.isMan = (Boolean.valueOf(cols[5]));
            this.dateBirth = (LocalDate.parse(cols[6]));
            this.phoneNumber = (cols[7]);
            this.dateMember = (LocalDateTime.parse(cols[8]));
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
