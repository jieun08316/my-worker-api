package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.board.BoardItem;
import com.episode6.myworkerapi.model.board.BoardResponse;
import com.episode6.myworkerapi.model.comment.CommentChangePostRequest;
import com.episode6.myworkerapi.model.comment.CommentItem;
import com.episode6.myworkerapi.model.comment.CommentRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "boardId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Board board;

    @JoinColumn(name = "memberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Member member;

    @Column(nullable = false,length = 400)
    private String content;

    @Column(nullable = false)
    private LocalDateTime dateContent;

    public void putComment(CommentChangePostRequest CommentChangePostRequest) {
        this.content = CommentChangePostRequest.getContent();
    }

    private Comment(Builder builder) {
        this.board = builder.board;
        this.member = builder.member;
        this.content = builder.content;
        this.dateContent = builder.dateContent;

    }


    public static class Builder implements CommonModelBuilder<Comment> {
        private final Board board;
        private final Member member;
        private final String content;
        private final LocalDateTime dateContent;

        public Builder(CommentRequest request,Member member,Board board) {
            this.board = board;
            this.member = member;
            this.content = request.getContent();
            this.dateContent = LocalDateTime.now();
        }
        @Override
        public Comment build() {
            return new Comment(this);
        }
    }
}
