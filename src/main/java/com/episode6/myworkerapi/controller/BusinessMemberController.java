package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.businessmember.*;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.BusinessMemberService;
import com.episode6.myworkerapi.service.BusinessService;
import com.episode6.myworkerapi.service.MemberService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/business-member")
public class BusinessMemberController {
    private final MemberService memberService;
    private final BusinessService businessService;
    private final BusinessMemberService businessMemberService;

    @PostMapping("/join/business-id/{businessId}/member-id/{memberId}")
    @Operation(summary = "사업장 직원 등록")
    public CommonResult setBusinessMember(@PathVariable long businessId, @PathVariable long memberId, @RequestBody BusinessMemberRequest request) {
        Business business = businessService.getBusinessData(businessId);
        Member member = memberService.getMemberData(memberId);
        businessMemberService.setBusinessMember(business, member, request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "사업장 직원 리스트로 보기 최신순 관리자용")
    public ListResult<BusinessMemberItem> getBusinessMemberAll(@PathVariable int pageNum) {
        return ResponseService.getListResult(businessMemberService.getBusinessMemberAll(pageNum), true);
    }

    @GetMapping("/all/business-id/{businessId}/{pageNum}")
    @Operation(summary = "내 사업장 재직자부터 퇴직자까지 리스트로 보기 사업자용")
    public ListResult<BusinessMemberItem> getBusinessOnlyMember(@PathVariable long businessId, @PathVariable int pageNum) {
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(businessMemberService.getBusinessOnlyMember(business, pageNum), true);
    }

    @GetMapping("/all/is-work/business-id/{businessId}/{pageNum}")
    @Operation(summary = "내 사업장 재직자만 리스트로 보기 사업자용")
    public ListResult<BusinessMemberItem> getBusinessOnlyMemberIsWork(@PathVariable long businessId, @PathVariable int pageNum) {
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(businessMemberService.getBusinessOnlyMemberIsWork(business, pageNum), true);
    }

    @GetMapping("/detail/business-member-id/{businessMemberId}")
    @Operation(summary = "사업장 알바생 상세보기")
    public SingleResult<BusinessMemberResponse> getBusinessMember(@PathVariable long businessMemberId) {
        return ResponseService.getSingleResult(businessMemberService.getBusinessMember(businessMemberId));
    }

    @PutMapping("/change/work/business-member-id/{businessMemberId}")
    @Operation(summary = "알바생 퇴직 재직 수정")
    public CommonResult putMemberChangeWork(@PathVariable long businessMemberId, @RequestBody BusinessMemberChangeWorkRequest request) {
        businessMemberService.putMemberChangeWork(businessMemberId, request);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change/schedule/business-member-id/{businessMemberId}")
    @Operation(summary = "알바생 근무일정 등록 및 수정")
    public CommonResult putMemberSchedule(@PathVariable long businessMemberId, @RequestBody BusinessMemberScheduleRequest request) {
        businessMemberService.putMemberSchedule(businessMemberId, request);
        return ResponseService.getSuccessResult();
    }
}
