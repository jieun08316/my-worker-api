package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.transition.TransitionItem;
import com.episode6.myworkerapi.model.transition.TransitionRequest;
import com.episode6.myworkerapi.model.transition.TransitionResponse;
import com.episode6.myworkerapi.service.BusinessMemberService;
import com.episode6.myworkerapi.service.BusinessService;
import com.episode6.myworkerapi.service.ResponseService;
import com.episode6.myworkerapi.service.TransitionService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/transition")
public class TransitionController {
    private final BusinessService businessService;
    private final BusinessMemberService businessMemberService;
    private final TransitionService transitionService;

    @PostMapping("/join/business-member-id/{businessMemberId}")
    @Operation(summary = "인수인계 등록")
    public CommonResult setTransition(@PathVariable long businessMemberId, @RequestBody TransitionRequest request) {
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);
        transitionService.setTransition(businessMember, request);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("/finish/transition-id/{transitionId}")
    @Operation(summary = "인수인계 내용 처리 완료 버튼")
    public CommonResult putFinish(@PathVariable long transitionId) {
        transitionService.putFinish(transitionId);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "인수인계 전체 리스트")
    public ListResult<TransitionItem> getTransitions(@PathVariable int pageNum) {
        return ResponseService.getListResult(transitionService.getTransitions(pageNum), true);
    }

    @GetMapping("/all/business-id/{businessId}/{pageNum}")
    @Operation(summary = "사업장 인수인계 리스트 최신순 페이징")
    public ListResult<TransitionItem> getTransitionBusiness(@PathVariable long businessId,@PathVariable int pageNum) {
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(transitionService.getTransitionBusiness(business, pageNum), true);
    }


    @GetMapping("/detail/transition-id/{transitionId}")
    @Operation(summary = "인수인계 상세보기")
    public SingleResult<TransitionResponse> getTransition(@PathVariable long transitionId) {
        return ResponseService.getSingleResult(transitionService.getTransition(transitionId));
    }

    @PutMapping("/change/transition-id/{transitionId}")
    @Operation(summary = "인수인계 내용 수정 해결된인수인계는 수정불가")
    public CommonResult putTransition(@PathVariable long transitionId, @RequestBody TransitionRequest request) {
        transitionService.putTransition(transitionId, request);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/transition-id/{transitionId}")
    @Operation(summary = "인수인계 삭제")
    public CommonResult delTransition(@PathVariable long transitionId) {
        transitionService.delTransition(transitionId);
        return ResponseService.getSuccessResult();
    }
}
