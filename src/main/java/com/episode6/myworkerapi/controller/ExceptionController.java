package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.exception.CMemberPasswordException;
import com.episode6.myworkerapi.model.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {

    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {// 로그인 하지 않았는데 로그인 한 회원만 사용 가능한 곳에 접근 했을때.
        throw new CMemberPasswordException();

    }

    @GetMapping("/entry-point")
    public CommonResult entryPointException() {//로그인 했지만 권한이 없는 곳에 접근 했을때

        throw new CMemberPasswordException();
    }

}
