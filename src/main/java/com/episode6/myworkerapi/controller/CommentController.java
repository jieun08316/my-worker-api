package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.comment.CommentChangePostRequest;
import com.episode6.myworkerapi.model.comment.CommentItem;
import com.episode6.myworkerapi.model.comment.CommentRequest;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/comment")
public class CommentController {
    private final CommentService commentService;
    private final MemberService memberService;
    private final BoardService boardService;
    private final DeleteService deleteService;

    @PostMapping("join/board-id/{boardId}/member-id/{memberId}")
    @Operation(summary = "댓글 등록")
    public CommonResult setComment(@RequestBody CommentRequest request, @PathVariable long memberId, @PathVariable long boardId ) {

        Member member = memberService.getMemberData(memberId);
        Board board = boardService.getBoardData(boardId);
        commentService.setComment(request, member, board);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("all/{pageNum}")
    @Operation(summary = "댓글 최신순 10개당 1페이지")
    public CommonResult getCommentPage(@PathVariable int pageNum) {

        return ResponseService.getListResult(commentService.getCommentPage(pageNum),true);
    }

    @GetMapping("detail/comment-id/{commentId}")
    @Operation(summary = "댓글 상세보기")
    public SingleResult<CommentItem> getComment(@PathVariable long commentId) {

        return ResponseService.getSingleResult(commentService.getComment(commentId));
    }

    @PutMapping("put/comment-id/{commentId}")
    @Operation(summary = "댓글 내용 수정")
    public CommonResult putComment(@RequestBody CommentChangePostRequest request, @PathVariable long commentId){

        commentService.putComment(request, commentId);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("del/comment-id/{commentId}")
    @Operation(summary = "댓글 삭제")
    public CommonResult delComment(@PathVariable long commentId) {

        deleteService.delComment(commentId);

        return ResponseService.getSuccessResult();
    }

}
