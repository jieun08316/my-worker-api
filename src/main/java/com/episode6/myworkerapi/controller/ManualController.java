package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.manual.ManualItem;
import com.episode6.myworkerapi.model.manual.ManualRequest;
import com.episode6.myworkerapi.model.manual.ManualResponse;
import com.episode6.myworkerapi.service.BusinessMemberService;
import com.episode6.myworkerapi.service.ManualService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/manual")
public class ManualController {
    private final ManualService manualService;
    private final BusinessMemberService businessMemberService;

    @PostMapping("join/business-member-id/{businessMemberId}")
    @Operation(summary = "메뉴얼 등록")
    public CommonResult setManual(@RequestBody ManualRequest request, @PathVariable long businessMemberId) {
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);
        manualService.setManual(request,businessMember);

        return ResponseService.getSuccessResult();
    }
    @GetMapping("all/business/{pageNum}/business-id/{businessId}")
    @Operation(summary = "메뉴얼 최신순 페이징 _사업장")
    public ListResult<ManualItem> getManuals(@PathVariable int pageNum ,@PathVariable long businessId) {

        return ResponseService.getListResult(manualService.getManuals(pageNum,businessId),true);
    }

    @GetMapping("all/{pageNum}")
    @Operation(summary = "메뉴얼 최신순 페이징 _관리자")
    public ListResult<ManualItem> getAllManuals(@PathVariable int pageNum) {
        return ResponseService.getListResult(manualService.getAllManuals(pageNum),true);
    }

    @GetMapping("detail/manual-id/{manualId}")
    @Operation(summary = "메뉴얼 상세보기")
    public SingleResult<ManualResponse> getManual(@PathVariable long manualId) {
        return ResponseService.getSingleResult(manualService.getManual(manualId));
    }
}
