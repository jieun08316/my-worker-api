package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.schedule.ScheduleItem;
import com.episode6.myworkerapi.model.schedule.ScheduleRequest;
import com.episode6.myworkerapi.service.BusinessMemberService;
import com.episode6.myworkerapi.service.ResponseService;
import com.episode6.myworkerapi.service.ScheduleService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/schedule")
public class ScheduleController {
    private final BusinessMemberService businessMemberService;
    private final ScheduleService scheduleService;

    @PostMapping("/join/business-member-id/{businessMemberId}")
    @Operation(summary = "출근 등록")
    public CommonResult setSchedule(@PathVariable long businessMemberId, @RequestBody ScheduleRequest request) {
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);
        scheduleService.setSchedule(businessMember, request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "출근 리스트 보기 관리자용")
    public ListResult<ScheduleItem> getSchedules(@PathVariable int pageNum) {
        return ResponseService.getListResult(scheduleService.getSchedules(pageNum), true);
    }
}
