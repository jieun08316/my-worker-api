package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.enums.MemberType;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.login.LoginRequest;
import com.episode6.myworkerapi.model.login.LoginResponse;
import com.episode6.myworkerapi.service.LoginService;
import com.episode6.myworkerapi.service.ResponseService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.login.LoginException;

@RestController
@RequestMapping("/v1/login")
@RequiredArgsConstructor
public class LoginController {
    private final LoginService loginService;
    @PostMapping("/web/admin")

    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_MANAGEMENT,loginRequest,"WEB"));
    }

    @PostMapping("/app/user")

    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_GENERAL,loginRequest,"APP"));
    }


    @PostMapping("/app/boss")
    public SingleResult<LoginResponse> doLoginBossApp(@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_BOSS,loginRequest,"APP"));
    }

    @PostMapping("/web/boss")
    public SingleResult<LoginResponse> doLoginBossWeb(@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_BOSS,loginRequest,"WEB"));
    }


}
