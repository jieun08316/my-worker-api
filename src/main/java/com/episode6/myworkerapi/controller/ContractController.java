package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.contract.ContractRequest;
import com.episode6.myworkerapi.model.contract.ContractResponse;
import com.episode6.myworkerapi.service.BusinessMemberService;
import com.episode6.myworkerapi.service.ContractService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/contract")
public class ContractController {
    private final ContractService contractService;
    private final BusinessMemberService businessMemberService;

    @PostMapping("join/business-memberId/{businessMemberId}")
    @Operation(summary = "계약 등록")
    public CommonResult setContract(@RequestBody ContractRequest contractRequest, @PathVariable long businessMemberId) {
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);

        contractService.setContract(businessMember,contractRequest);

        return ResponseService.getSuccessResult();
    }
    @GetMapping("detail/contract-id/{contractId}")
    @Operation(summary = "계약서 자세히 보기")
    public SingleResult<ContractResponse> getContract(@PathVariable long contractId) {
        return ResponseService.getSingleResult(contractService.getContract(contractId));
    }
    @GetMapping("all/{pageNum}")
    @Operation(summary = "계약서 페이징 최신순")
    public ListResult<ContractResponse> getContracts(int pageNum) {
        return contractService.getContracts(pageNum);
    }
}
